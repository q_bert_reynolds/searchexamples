﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckerMove {
    public bool p1Turn;
    public int fromX;
    public int fromY;
    public int toX;
    public int toY;
    public int[,] boardState;
    public CheckerMove parent;

    public CheckerMove (int[,] boardState, bool p1Turn) {
        fromX = -1;
        fromY = -1;
        toX = -1;
        toY = -1;
        parent = null;
        this.boardState = boardState;
        this.p1Turn = p1Turn;
    }

    public CheckerMove (int fromX, int fromY, int toX, int toY, CheckerMove move) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        parent = move;
        p1Turn = !parent.p1Turn;
        boardState = (int[,])move.boardState.Clone();
        boardState[fromX, fromY] = 0;
        boardState[toX, toY] = 1;
        boardState[(fromX+toX)/2, (fromY+toY)/2] = 0;
    }

    public bool HasMoves () {
        int width = boardState.GetLength(0);
        int height = boardState.GetLength(1);

        int dirY = p1Turn ? 1 : -1;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int checker = boardState[i,j];
                if (checker == 0 || dirY * checker < 0) continue;
                
                if (IsLegalMove(i, j, i+1, j+dirY)) return true;
                if (IsLegalMove(i, j, i-1, j+dirY)) return true;
                if (IsLegalMove(i, j, i+2, j+dirY*2)) return true;
                if (IsLegalMove(i, j, i-2, j+dirY*2)) return true;

                // if kinged, move opposite direction as well
                if (Mathf.Abs(checker) > 1) {
                    if (IsLegalMove(i, j, i+1, j-dirY)) return true;
                    if (IsLegalMove(i, j, i-1, j-dirY)) return true;
                    if (IsLegalMove(i, j, i+2, j-dirY*2)) return true;
                    if (IsLegalMove(i, j, i-2, j-dirY*2)) return true;
                }
            }
        }
        return false;
    }

    public List<CheckerMove> GetMoves () {
        List<CheckerMove> moves = new List<CheckerMove>();
        int width = boardState.GetLength(0);
        int height = boardState.GetLength(1);

        int dirY = p1Turn ? 1 : -1;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int checker = boardState[i,j];
                if (checker == 0 || dirY * checker < 0) continue;
                
                TryAddMove(i, j, i+1, j+dirY,   moves);
                TryAddMove(i, j, i-1, j+dirY,   moves);
                TryAddMove(i, j, i+2, j+dirY*2, moves);
                TryAddMove(i, j, i-2, j+dirY*2, moves);

                // if kinged, move opposite direction as well
                if (Mathf.Abs(checker) > 1) {
                    TryAddMove(i, j, i+1, j-dirY,   moves);
                    TryAddMove(i, j, i-1, j-dirY,   moves);
                    TryAddMove(i, j, i+2, j-dirY*2, moves);
                    TryAddMove(i, j, i-2, j-dirY*2, moves);
                }
            }
        }
        return moves;
    }

    void TryAddMove (int fromX, int fromY, int toX, int toY, List<CheckerMove> moves) {
        if (IsLegalMove(fromX, fromY, toX, toY)) {
            CheckerMove move = new CheckerMove(fromX, fromY, toX, toY, this);
            moves.Add(move);
        }
    }

    public bool IsLegalMove (int fromX, int fromY, int toX, int toY) {
        // odd space
        if ((fromX+fromY) % 2 == 1 || (toX+toY) % 2 == 1) return false;

        // out of bounds
        int width = boardState.GetLength(0);
        int height = boardState.GetLength(1);
        if (fromX < 0 || fromY < 0 || toX < 0 || toY < 0) return false;
        if (fromX >= width || fromY >= height || toX >= width || toY >= height) return false;
        
        // target space occupied
        if (boardState[toX, toY] != 0) return false;
        
        // nothing to move
        int checker = boardState[fromX, fromY];
        if (checker == 0) return false;

        // not your turn
        if ((checker > 0) != p1Turn) return false;

        // wrong direction 
        int dX = toX - fromX;
        int dY = toY - fromY;
        if (checker * dY < 0 && Mathf.Abs(checker) < 2) return false;
        
        // not diagonal, too far
        int absX = Mathf.Abs(dX);
        int absY = Mathf.Abs(dY);
        if (absX != absY || absX == 0 || absX > 2) return false;

        // nothing to jump
        int jump = boardState[(fromX+toX)/2, (fromY+toY)/2];
        if (absX == 2 && (jump == 0 || jump * checker > 0)) return false;
        
        // all good
        return true;
    }

    public bool IsTerminal () {
        int width = boardState.GetLength(0);
        int height = boardState.GetLength(1);
        int lastType = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int checker = boardState[i, j];
                if (checker == 0) continue;
                if (lastType == 0) lastType = checker;
                else if (lastType != checker) return false;
            }
        }
        Debug.Log("FOUND TERMINAL NODE");
        return true;
    }

    public int Heuristic () {
        int sum = 0;
        foreach (int checker in boardState) {
            sum += checker;
        }
        return sum;
    }

    public int GetPieceCount () {
        int sum = 0;
        foreach (int checker in boardState) {
            if (checker != 0) sum++;
        }
        return sum;
    }

    public CheckerMove MinMax () {
        int width = boardState.GetLength(0);
        int height = boardState.GetLength(1) - 2;
        int maxPieces = (width * height) / 2;
        int pieceCount = GetPieceCount();
        float t = (float)pieceCount / (float)maxPieces;
        t = Mathf.Clamp01((t - 0.5f) * 2);
        int maxDepth = (int)Mathf.Lerp(12, 6, t);
        Debug.Log(maxDepth);

        List<CheckerMove> moves = GetMoves();
        CheckerMove bestMove = moves[0];
        int bestScore = p1Turn ? int.MinValue : int.MaxValue;
        foreach (CheckerMove child in moves) {
            int score = AlphaBeta(child, maxDepth, int.MinValue, int.MaxValue, !p1Turn);
            if ((p1Turn && score > bestScore) || (!p1Turn && score < bestScore)) {
                bestScore = score;
                bestMove = child;
            }
        }
        return bestMove;
    }

    public int MinMax(CheckerMove move, int depth, bool isMax) {
        if (depth == 0 || move.IsTerminal()) {
            return move.Heuristic();
        }

        List<CheckerMove> moves = move.GetMoves();
        int score = isMax ? int.MinValue : int.MaxValue;
        foreach (CheckerMove child in moves) {
            int s = MinMax(child, depth-1, !isMax);
            if (isMax) score = Mathf.Max(score, s);
            else score = Mathf.Min(score, s);
        }
        return score;
    }

    public int AlphaBeta(CheckerMove move, int depth, int alpha, int beta, bool isMax) {
        if (depth == 0 || move.IsTerminal()) {
            return move.Heuristic();
        }

        List<CheckerMove> moves = move.GetMoves();
        int score = isMax ? int.MinValue : int.MaxValue;
        foreach (CheckerMove child in moves) {
            int s = AlphaBeta(child, depth-1, alpha, beta, !isMax);
            if (isMax) {
                score = Mathf.Max(score, s);
                alpha = Mathf.Max(alpha, score);
            }
            else {
                score = Mathf.Min(score, s);
                beta = Mathf.Min(beta, score);
            }
            if (alpha >= beta) break;
        }
        return score;
    }
}
