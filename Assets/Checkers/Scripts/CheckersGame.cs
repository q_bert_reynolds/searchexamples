﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckersGame : MonoBehaviour {

    public int width = 8;
    public int height = 8;
    public GameObject[,] checkers;
    bool p1Turn = true;

    public Material p1Material;
    public Material p2Material;
    public Material evenMaterial;
    public Material oddMaterial;

    public GameObject checkerPrefab;
    public GameObject tilePrefab;

    public Text text;
    bool gameOver = false;
    void Start () {
        Setup();
    }

    [ContextMenu("Setup")]
    void Setup () {
        gameOver = false;
        // text.enabled = false;
        for (int i = transform.childCount-1; i >= 0; i--) {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        checkers = new GameObject[width,height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                bool even = ((i+j) % 2 == 0);
                if (even && (j < 3 || j >= height - 3)) {
                    GameObject checker = Instantiate(checkerPrefab);
                    checker.transform.SetParent(transform);
                    checker.transform.position = new Vector3(i,j,0);
                    checker.transform.localEulerAngles = Vector3.forward * ((j<3) ? 0 : 180);
                    MeshRenderer checkerRenderer = checker.GetComponentInChildren<MeshRenderer>();
                    checkerRenderer.sharedMaterial = (j<3) ? p1Material : p2Material;
                    checkers[i,j] = checker;
                }
                GameObject tile = Instantiate(tilePrefab);
                tile.transform.position = new Vector3(i,j,0);
                tile.transform.SetParent(transform);
                MeshRenderer tileRenderer = tile.GetComponent<MeshRenderer>();
                tileRenderer.sharedMaterial = even ? evenMaterial : oddMaterial;
            }
        }
        Camera.main.transform.position = new Vector3(
            (float)width * 0.5f - 0.5f,
            (float)height * 0.5f - 0.5f,
            (float)(width+height) * -0.5f
        );
    }

    int startX;
    int startY;
    Vector3 startPos;
    Vector3 offset;
    float startDist;
    Transform grabbedPeg;
    Plane plane = new Plane(Vector3.forward, Vector3.zero);
    void Update () {
        if (gameOver || !p1Turn) return;
        CheckerMove current = new CheckerMove(GetCurrentBoardState(), true);
        if (!current.HasMoves()) {
            p1Turn = false;
        }

        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("Player")) {
                grabbedPeg = hit.collider.transform;
                startPos = grabbedPeg.position;
                startX = Mathf.RoundToInt(startPos.x);
                startY = Mathf.RoundToInt(startPos.y);
                offset =  hit.point - startPos;
                startDist = hit.distance;
            }
        }
        else if (Input.GetMouseButton(0) && grabbedPeg != null) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float dist;
            plane.Raycast(ray, out dist);
            float d = Vector2.Distance(grabbedPeg.transform.position, startPos);
            d = Mathf.Lerp(dist, dist - 0.1f, Mathf.Clamp01(d / 0.5f));
            grabbedPeg.position = ray.GetPoint(d) + offset;
        }
        else if (Input.GetMouseButtonUp(0) && grabbedPeg != null) {
            int endX = Mathf.RoundToInt(grabbedPeg.position.x);
            int endY = Mathf.RoundToInt(grabbedPeg.position.y);
            if (current.IsLegalMove(startX, startY, endX, endY)) {
                grabbedPeg.position = new Vector3(endX, endY, 0);
                checkers[startX, startY] = null;
                checkers[endX, endY] = grabbedPeg.gameObject;

                if (Mathf.Abs(endX-startX) == 2) {
                    GameObject jumppedPeg = checkers[(startX+endX)/2, (startY+endY)/2];
                    jumppedPeg.SetActive(false);
                    checkers[(startX+endX)/2, (startY+endY)/2] = null;
                }

                if ((endY == 0 || endY == height-1) && grabbedPeg.localScale.z < 2) {
                    grabbedPeg.localScale = new Vector3(1,1,3);
                }

                p1Turn = false;
                StartCoroutine("AutoMove");
            }
            else grabbedPeg.position = startPos;
            grabbedPeg = null;
        }
    }

    int[,] GetCurrentBoardState () {
        int[,] boardState = new int[width,height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                GameObject c = checkers[i,j];
                int team = (c == null) ? 0 : (int)c.transform.up.y;
                if (team != 0 && c.transform.localScale.z > 1) team *= 2;
                boardState[i,j] = team;
            }
        }
        return boardState;
    }

    IEnumerator AutoMove () {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        CheckerMove current = new CheckerMove(GetCurrentBoardState(), false);
        if (!current.HasMoves()) {
            Debug.Log("NO MOVES LEFT");
            yield break;
        }
        CheckerMove move = current.MinMax();
        if (move == null) {
            Debug.Log("MIN MAX FAIL!!!");
            yield break;
        }
        GameObject checker = checkers[move.fromX, move.fromY];
        for (float t = 0; t < 0.5f; t += Time.deltaTime) {
            float frac = t / 0.5f;
            float h = -0.5f * Mathf.Cos(t * Mathf.PI);
            checker.transform.position = Vector3.Lerp(
                new Vector3(move.fromX, move.fromY, h),
                new Vector3(move.toX, move.toY, h),
                frac
            );
            yield return new WaitForEndOfFrame();
        }

        checker.transform.position = new Vector3(move.toX, move.toY, 0);
        checkers[move.fromX, move.fromY] = null;
        checkers[move.toX, move.toY] = checker;
        if ((move.toY == 0 || move.toY == height-1) && checker.transform.localScale.z < 2) {
            checker.transform.localScale = new Vector3(1,1,3);
        }

        if (Mathf.Abs(move.toX - move.fromX) > 1) {
            GameObject jump = checkers[(move.fromX+move.toX)/2, (move.fromY+move.toY)/2];
            jump.SetActive(false);
            checkers[(move.fromX+move.toX)/2, (move.fromY+move.toY)/2] = null;
        }

        yield return new WaitForSeconds(0.1f);
        p1Turn = true;
    }
}
