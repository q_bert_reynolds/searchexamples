﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PathExtensions {
    public static void DijkstraInsert (this List<PathNode> moves, PathNode move) {
        int len = moves.Count;
        for (int i = 0; i < len; i++) {
            PathNode other = moves[i];
            if (move.depth < other.depth) {
                moves.Insert(i, move);
                return;
            }
        }
        moves.Add(move);
    }
    
    public static void AStarInsert (this List<PathNode> moves, PathNode move) {
        int len = moves.Count;
        for (int i = 0; i < len; i++) {
            PathNode other = moves[i];
            if (move.score < other.score) {
                moves.Insert(i, move);
                return;
            }
        }
        moves.Add(move);
    }
}

public class PathNode : System.Object {

    public bool scheduled;
    public bool visited;
    public Rect bounds;
    public int depth;
    public int score;
    public int x;
    public int y;
    public PathNode parent;

    public static Dictionary<int, PathNode> allMoves;

    public Vector2 pos {
        get { return new Vector2(x,y); }
    }

    public PathNode (Vector2 start, Rect bounds) {
        x = Mathf.RoundToInt(start.x);
        y = Mathf.RoundToInt(start.y);
        parent = null;
        depth = 0;
        score = int.MaxValue;
        visited = false;
        this.bounds = bounds;
        allMoves = new Dictionary<int, PathNode>();
        allMoves[GetHashCode()] = this;
    }

    public PathNode (int dirX, int dirY, PathNode parent) {
        x = parent.x + dirX;
        y = parent.y + dirY;
        this.parent = parent;
        depth = parent.depth + 1;
        score = int.MaxValue;
        visited = false;
        bounds = parent.bounds;
        allMoves[GetHashCode()] = this;
    }

    public int Heuristic (int goalX, int goalY) {
        return Mathf.RoundToInt(new Vector2(goalX-x, goalY-y).magnitude);
    }

    public override bool Equals (System.Object o) {
        if ((o == null) || !(o is PathNode)) return false;
        PathNode p = o as PathNode; 
        return x == p.x && y == p.y;
    }

    public override int GetHashCode() {
        return x * 1000000 + y;
    }

    public List<PathNode> GetMoves () {
        List<PathNode> moves = new List<PathNode>();
        if (CanMove(-1, 0)) moves.Add(GetMove(-1, 0));
        if (CanMove( 1, 0)) moves.Add(GetMove( 1, 0));
        if (CanMove( 0, 1)) moves.Add(GetMove( 0, 1));
        if (CanMove( 0,-1)) moves.Add(GetMove( 0,-1));
        return moves;
    }

    public PathNode GetMove (int dirX, int dirY) {
        int posX = x + dirX;
        int posY = y + dirY;
        int key = posX * 1000000 + posY;

        PathNode move;
        if (allMoves.ContainsKey(key)) move = allMoves[key];
        else move = new PathNode(dirX, dirY, this);

        return move;
    }

    static Collider2D[] c = new Collider2D[10];
    public bool CanMove (int dirX, int dirY) {
        int posX = x + dirX;
        int posY = y + dirY;
        if (parent != null && parent.x == posX && parent.y == posY) return false;

        Vector2 pos = new Vector2(posX, posY);
        if (!bounds.Contains(pos)) return false;
        
        int num = Physics2D.OverlapCircleNonAlloc(pos, 0.5f, c);
        for (int i = 0; i < num; i++) {
            if (!c[i].CompareTag("Player")) return false;
        }
        
        return true;
    }

    public Vector2[] GetPath () {
        List<PathNode> moves = new List<PathNode>();
        PathNode move = this;
        while (move != null) {
            moves.Insert(0, move);
            move = move.parent;
        }
        return moves.ConvertAll(m => m.pos).ToArray();
    }

    public static Vector2[] path;

    public IEnumerator DepthFirstSearch (Vector2 target) {
        int x = Mathf.RoundToInt(target.x);
        int y = Mathf.RoundToInt(target.y);
        Stack<PathNode> moveStack = new Stack<PathNode>();
        moveStack.Push(this);
        while (moveStack.Count > 0) {
            PathNode move = moveStack.Pop();
            
            if (move.x == x && move.y == y) {
                path = move.GetPath();
                yield break;
            }
            
            move.visited = true;
            List<PathNode> moves = move.GetMoves();
            foreach (PathNode m in moves) {
                if (!m.scheduled) {
                    moveStack.Push(m);
                    m.scheduled = true;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator BreadthFirstSearch (Vector2 target) {
        int x = Mathf.RoundToInt(target.x);
        int y = Mathf.RoundToInt(target.y);
        Queue<PathNode> moveQueue = new Queue<PathNode>();
        moveQueue.Enqueue(this);
        while (moveQueue.Count > 0) {
            PathNode move = moveQueue.Dequeue();
            
            if (move.x == x && move.y == y) {
                path = move.GetPath();
                yield break;
            }
            
            move.visited = true;
            List<PathNode> moves = move.GetMoves();
            foreach (PathNode m in moves) {
                if (!m.scheduled) {
                    moveQueue.Enqueue(m);
                    m.scheduled = true;
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator DijkstraSearch (Vector2 target) {
        int x = Mathf.RoundToInt(target.x);
        int y = Mathf.RoundToInt(target.y);
        List<PathNode> moveList = new List<PathNode>();
        moveList.Add(this);
        
        while (moveList.Count > 0) {
            PathNode move = moveList[0];
            moveList.RemoveAt(0);

            if (move.x == x && move.y == y) {
                path = move.GetPath();
                yield break;
            }
            
            move.visited = true;
            List<PathNode> moves = move.GetMoves();
            foreach (PathNode m in moves) {
                if (m.scheduled && move.depth + 1 < m.depth) {
                    moveList.Remove(m);
                    m.depth = move.depth+1;
                    m.parent = move;
                    m.visited = false;
                    m.scheduled = false;
                }
                if (!m.scheduled) {
                    moveList.DijkstraInsert(m);
                    m.scheduled = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator AStarSearch (Vector2 target) {
        int x = Mathf.RoundToInt(target.x);
        int y = Mathf.RoundToInt(target.y);
        HashSet<PathNode> closedSet = new HashSet<PathNode>();
        List<PathNode> openSet = new List<PathNode>();
        openSet.Add(this);
        
        while (openSet.Count > 0) {
            PathNode move = openSet[0];
            openSet.RemoveAt(0);

            if (move.x == x && move.y == y) {
                path = move.GetPath();
                yield break;
            }
            
            move.visited = true;
            closedSet.Add(move);
            List<PathNode> moves = move.GetMoves();
            foreach (PathNode m in moves) {
                if (closedSet.Contains(m)) continue;

                int score = move.depth + 1;
                if (m.scheduled) {
                    if (score >= m.depth) continue;
                    openSet.Remove(m);
                }

                m.parent = move;
                m.depth = score;
                m.score = score + m.Heuristic(x,y);
                if (!m.scheduled) {
                    openSet.AStarInsert(m);
                    m.scheduled = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }
    }
}
