﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : MonoBehaviour {

    public Rect bounds = new Rect(-20,-10,40,20);
    public enum SearchType {
        BreadthFirst,
        DepthFirst,
        Dijkstras,
        AStar
    }
    public SearchType searchType = SearchType.BreadthFirst;
    public float speed = 3;
    public float arriveDist = 0.1f;

    Vector2[] path {
        get { return PathNode.path; }
        set { PathNode.path = value; }
    }
    
    int index;

    Rigidbody2D body;
    void Start () {
        body = GetComponent<Rigidbody2D>();
    }

    void Update () {
        if (Input.GetMouseButtonDown(0)) {
            Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            UpdatePath(target);
        }

        if (path == null || path.Length == 0) return;

        Vector2 diff = path[index] - body.position;
        body.velocity = diff.normalized * speed;

        if (diff.sqrMagnitude < arriveDist * arriveDist) index++;

        if (index >= path.Length) {
            body.velocity = Vector2.zero;
            path = null;
            index = 0;
        }
    }

    public void UpdatePath (Vector2 target) {
        PathNode start = new PathNode(body.position, bounds);
        StopAllCoroutines();
        path = null;
        body.velocity = Vector2.zero;
        switch (searchType) {
            case SearchType.BreadthFirst:
                StartCoroutine(start.BreadthFirstSearch(target));
                break;
            case SearchType.DepthFirst:
                StartCoroutine(start.DepthFirstSearch(target));
                break;
            case SearchType.Dijkstras:
                StartCoroutine(start.DijkstraSearch(target));
                break;
            case SearchType.AStar:
                StartCoroutine(start.AStarSearch(target));
                break;
            default:
                break;
        }
        
        index = 0;
    }

    void OnDrawGizmos () {
        Gizmos.DrawWireCube(bounds.center, bounds.size);

        if (PathNode.allMoves == null) return;
        foreach (PathNode node in PathNode.allMoves.Values) {
            Gizmos.color = node.visited ? Color.white : Color.blue;
            Gizmos.DrawSphere(node.pos, 0.3f);
        }
    }
}
