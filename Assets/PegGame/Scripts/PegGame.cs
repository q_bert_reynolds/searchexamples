﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PegGame : MonoBehaviour {

    public int width = 5;
    public int height = 5;
    public GameObject[,] pegs;

    public GameObject pegPrefab;
    public GameObject holePrefab;

    public Text text;
    bool gameOver = false;
    bool auto = false;
    void Start () {
        Setup();
    }

    [ContextMenu("AutoSolve")]
    void AutoSolve () {
        auto = true;
        PegMove root = new PegMove(GetCurrentPegState());
        PegMove solution = root.BestFirstSearch();
        if (solution != null) {
            List<PegMove> path = solution.GetPath();
            StartCoroutine("AutoSolveCoroutine", path);
        }
    }

    IEnumerator AutoSolveCoroutine(List<PegMove> path) {
        foreach (PegMove move in path) {
            if (move.fromX == -1) continue;

            GameObject peg = pegs[move.fromX, move.fromY];
            for (float t = 0; t < 0.5f; t += Time.deltaTime) {
                float frac = t / 0.5f;
                float h = -0.5f * Mathf.Cos(t * Mathf.PI);
                peg.transform.position = Vector3.Lerp(
                    new Vector3(move.fromX, move.fromY, h),
                    new Vector3(move.toX, move.toY, h),
                    frac
                );
                yield return new WaitForEndOfFrame();
            }

            peg.transform.position = new Vector3(move.toX, move.toY, 0);
            pegs[move.fromX, move.fromY] = null;
            GameObject jumppedPeg = pegs[(move.fromX+move.toX)/2, (move.fromY+move.toY)/2];
            jumppedPeg.SetActive(false);
            pegs[(move.fromX+move.toX)/2, (move.fromY+move.toY)/2] = null;
            pegs[move.toX, move.toY] = peg;
            yield return new WaitForSeconds(0.1f);
        }

        if (!HasMovesLeft()) {
            gameOver = true;
            text.text = string.Format("GAME OVER\n{0} PIECES LEFT", PiecesOnBoard());
            text.enabled = true;
        }
        else {
            AutoSolve();
        }
    }

    [ContextMenu("Setup")]
    void Setup () {
        auto = false;
        gameOver = false;
        text.enabled = false;
        for (int i = transform.childCount-1; i >= 0; i--) {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        GameObject hole = Instantiate(holePrefab);
        hole.transform.position = new Vector3(0,0,0);
        hole.transform.SetParent(transform);
        pegs = new GameObject[width,height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (i == 0 && j == 0) continue;
                GameObject peg = Instantiate(pegPrefab);
                peg.transform.SetParent(transform);
                peg.transform.position = new Vector3(i,j,0);
                pegs[i,j] = peg;
                hole = Instantiate(holePrefab);
                hole.transform.position = new Vector3(i,j,0);
                hole.transform.SetParent(transform);
            }
        }
        Camera.main.transform.position = new Vector3(
            (float)width * 0.5f - 0.5f,
            (float)height * 0.5f - 0.5f,
            (float)(width+height) * -0.5f
        );
    }

    public int PiecesOnBoard () {
        int count = 0;
        foreach (GameObject peg in pegs) {
            if (peg != null) count++;
        }
        return count;
    }

    public bool HasMovesLeft () {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (pegs[i,j] == null) continue;
                if (IsLegalMove(i, j, i+2, j)) return true;
                if (IsLegalMove(i, j, i-2, j)) return true;
                if (IsLegalMove(i, j, i, j+2)) return true;
                if (IsLegalMove(i, j, i, j-2)) return true;
            }
        }
        return false;
    }

    public bool IsLegalMove (int fromX, int fromY, int toX, int toY) {
        if (fromX < 0 || fromY < 0 || toX < 0 || toY < 0) return false;
        if (fromX >= width || fromY >= height || toX >= width || toY >= height) return false;
        if (pegs[toX, toY] != null) return false;
        if (pegs[fromX, fromY] == null) return false;
        int dX = toX - fromX;
        int dY = toY - fromY;
        int absX = Mathf.Abs(dX);
        int absY = Mathf.Abs(dY);
        if (!(absX == 2 && absY == 0) && !(absY == 2 && absX == 0)) return false;
        if (pegs[(fromX+toX)/2, (fromY+toY)/2] == null) return false;
        return true;
    }

    int startX;
    int startY;
    Vector3 startPos;
    Vector3 offset;
    float startDist;
    Transform grabbedPeg;
    void Update () {
        if (gameOver || auto) return;

        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.CompareTag("Peg")) {
                grabbedPeg = hit.collider.transform;
                startPos = grabbedPeg.position;
                startX = Mathf.RoundToInt(startPos.x);
                startY = Mathf.RoundToInt(startPos.y);
                offset =  hit.point - startPos;
                startDist = hit.distance;
            }
        }
        else if (Input.GetMouseButton(0) && grabbedPeg != null) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float d = Vector2.Distance(grabbedPeg.transform.position, startPos);
            d = Mathf.Lerp(startDist, startDist - 0.1f, Mathf.Clamp01(d / 0.5f));
            grabbedPeg.position = ray.GetPoint(d) + offset;
        }
        else if (Input.GetMouseButtonUp(0) && grabbedPeg != null) {
            int endX = Mathf.RoundToInt(grabbedPeg.position.x);
            int endY = Mathf.RoundToInt(grabbedPeg.position.y);
            if (IsLegalMove(startX, startY, endX, endY)) {
                grabbedPeg.position = new Vector3(endX, endY, 0);
                pegs[startX, startY] = null;
                GameObject jumppedPeg = pegs[(startX+endX)/2, (startY+endY)/2];
                jumppedPeg.SetActive(false);
                pegs[(startX+endX)/2, (startY+endY)/2] = null;
                pegs[endX, endY] = grabbedPeg.gameObject;
            }
            else grabbedPeg.position = startPos;
            grabbedPeg = null;

            if (!HasMovesLeft()) {
                gameOver = true;
                text.text = string.Format("GAME OVER\n{0} PIECES LEFT", PiecesOnBoard());
                text.enabled = true;
            }
        }
    }

    int[,] GetCurrentPegState () {
        int[,] pegState = new int[width,height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pegState[i,j] = (pegs[i,j] == null) ? 0 : 1;
            }
        }
        return pegState;
    }
}
