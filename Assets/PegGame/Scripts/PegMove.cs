﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PegMove {
    public int compactness;
    public int depth;
    public int fromX;
    public int fromY;
    public int toX;
    public int toY;
    public int[,] pegState;
    public PegMove parent;

    public PegMove (int[,] pegState) {
        depth = 0;
        fromX = -1;
        fromY = -1;
        toX = -1;
        toY = -1;
        parent = null;
        this.pegState = pegState;
        compactness = GetCompactness();
    }

    public PegMove (int fromX, int fromY, int toX, int toY, PegMove move) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        depth = move.depth+1;
        parent = move;
        pegState = (int[,])move.pegState.Clone();
        pegState[fromX, fromY] = 0;
        pegState[toX, toY] = 1;
        pegState[(fromX+toX)/2, (fromY+toY)/2] = 0;
        compactness = GetCompactness();
    }

    public int GetCompactness () {
        int compactness = 0;
        int width = pegState.GetLength(0);
        int height = pegState.GetLength(1);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int peg = pegState[i,j];
                if (peg == 0) continue;
                int found = 0;
                int possible = 0;
                for (int x = i-1; x < i+2; x++) {
                    for (int y = j-1; y < j+2; y++) {
                        if (x >= 0 && y >=0 && x < width && y < height) {
                            possible++;
                            if (pegState[x,y] == 1) found++;
                        }
                    }
                }
                int score = found * 2 - possible;
                if (score < 0) score *= 2;
                compactness += score;
            }
        }
        return compactness;
    }

    public int PiecesOnBoard () {
        int count = 0;
        foreach (int peg in pegState) {
            count += peg;
        }
        return count;
    }

    int maxChildren = 3;
    int maxDepth = 8;
    public List<PegMove> GetMoves () {
        List<PegMove> moves = new List<PegMove>();
        int width = pegState.GetLength(0);
        int height = pegState.GetLength(1);
        
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (pegState[i,j] == 0) continue;
                
                if (IsLegalMove(i, j, i+2, j)) {
                    moves.Add(new PegMove(i, j, i+2, j, this));
                }

                if (IsLegalMove(i, j, i-2, j)) {
                    moves.Add(new PegMove(i, j, i-2, j, this));
                }

                if (IsLegalMove(i, j, i, j+2)) {
                    moves.Add(new PegMove(i, j, i, j+2, this));
                }

                if (IsLegalMove(i, j, i, j-2)) {
                    moves.Add(new PegMove(i, j, i, j-2, this));
                }
            }
        }
        moves.Sort((p1,p2) => p2.compactness.CompareTo(p1.compactness));
        if (moves.Count > maxChildren) return moves.GetRange(0, maxChildren);
        return moves;
    }

    public bool IsLegalMove (int fromX, int fromY, int toX, int toY) {
        int width = pegState.GetLength(0);
        int height = pegState.GetLength(1);
        if (fromX < 0 || fromY < 0 || toX < 0 || toY < 0) return false;
        if (fromX >= width || fromY >= height || toX >= width || toY >= height) return false;
        if (pegState[toX, toY] == 1) return false;
        if (pegState[fromX, fromY] == 0) return false;
        int dX = toX - fromX;
        int dY = toY - fromY;
        int absX = Mathf.Abs(dX);
        int absY = Mathf.Abs(dY);
        if (!(absX == 2 && absY == 0) && !(absY == 2 && absX == 0)) return false;
        if (pegState[(fromX+toX)/2, (fromY+toY)/2] == 0) return false;
        return true;
    }

    public PegMove DepthFirstSearch () {
        Stack<PegMove> moveStack = new Stack<PegMove>();
        moveStack.Push(this);
        PegMove bestMove = null;
        int pegsLeft = int.MaxValue;
        while (moveStack.Count > 0) {
            PegMove move = moveStack.Pop();
            if (move.depth < maxDepth) {
                List<PegMove> moves = move.GetMoves();
                if (moves.Count > 0) foreach(PegMove m in moves) moveStack.Push(m);
            }            
            if (move.PiecesOnBoard() < pegsLeft) {
                bestMove = move;
                pegsLeft = move.PiecesOnBoard();
                if (pegsLeft == 1) return move;
            }
        }
        return bestMove;
    }

    public PegMove BreadthFirstSearch () {
        Queue<PegMove> moveQueue = new Queue<PegMove>();
        moveQueue.Enqueue(this);
        PegMove bestMove = null;
        int pegsLeft = int.MaxValue;
        while (moveQueue.Count > 0) {
            PegMove move = moveQueue.Dequeue();
            if (move.depth < maxDepth) {
                List<PegMove> moves = move.GetMoves();
                if (moves.Count > 0) foreach(PegMove m in moves) moveQueue.Enqueue(m);
            }
            if (move.PiecesOnBoard() < pegsLeft) {
                bestMove = move;
                pegsLeft = move.PiecesOnBoard();
                if (pegsLeft == 1) return move;
            }
        }
        return bestMove;
    }

    public PegMove BestFirstSearch () {
        List<PegMove> moveList = new List<PegMove>();
        moveList.Add(this);
        PegMove bestMove = null;
        int pegsLeft = int.MaxValue;
        while (moveList.Count > 0) {
            PegMove move = moveList[0];
            moveList.RemoveAt(0);
            if (move.depth < maxDepth) {
                List<PegMove> moves = move.GetMoves();
                if (moves.Count > 0) {
                    foreach(PegMove m in moves) moveList.Add(m);
                    moves.Sort((p1,p2) => p2.compactness.CompareTo(p1.compactness));
                }
            }
            if (move.PiecesOnBoard() < pegsLeft) {
                bestMove = move;
                pegsLeft = move.PiecesOnBoard();
                if (pegsLeft == 1) return move;
            }
        }
        return bestMove;
    }

    public List<PegMove> GetPath () {
        List<PegMove> moves = new List<PegMove>();
        PegMove move = this;
        while (move != null) {
            moves.Insert(0, move);
            move = move.parent;
        }
        return moves;
    }
}
